/**
 * 
 */
package plugins.fab.trackmanager.processors;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;

import java.io.File;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.fab.trackmanager.TrackGroup;

/**
 * Block (Protocols) to export Tracks (TrackGroup) in XLS format.
 * 
 * @author Stephane
 */
public class TrackXLSExport extends Plugin implements Block, PluginBundled
{
    public final VarMutable file;
    public final Var<TrackGroup> tracks;

    public TrackXLSExport()
    {
        super();

        file = new VarMutable("Output file", null)
        {
            @Override
            public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
            {
                return (String.class == source.getType()) || (File.class == source.getType());
            }
        };
        tracks = new Var<TrackGroup>("TrackGroup", new TrackGroup(null));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("tracks", tracks);
        inputMap.add("file", file);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        //
    }

    @Override
    public void run()
    {
        final Object obj = file.getValue();
        final TrackGroup tg = tracks.getValue();

        if ((obj != null) && (tg != null))
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            TrackProcessorExportTrackToXLS.exportToXLS(tg.getTrackSegmentList(), f);
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return TrackProcessorExportTrackToXLS.class.getName();
    }
}
