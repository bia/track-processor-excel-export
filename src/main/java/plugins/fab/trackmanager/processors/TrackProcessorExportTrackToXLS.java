package plugins.fab.trackmanager.processors;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import icy.gui.dialog.MessageDialog;
import icy.gui.util.GuiUtil;
import icy.main.Icy;
import icy.system.IcyExceptionHandler;
import icy.util.XLSUtil;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;

public class TrackProcessorExportTrackToXLS extends PluginTrackManagerProcessor implements ActionListener
{
    public TrackProcessorExportTrackToXLS()
    {
        JButton exportTracksToXLSButton = new JButton("export tracks to excel");
        setName("Export Tracks to XLS");

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(GuiUtil.createLineBoxPanel(exportTracksToXLSButton));
        exportTracksToXLSButton.addActionListener(this);
    }

    @Override
    public void Close()
    {
        //
    }

    @Override
    public void Compute()
    {
        //
    }

    @Override
    public void displaySequenceChanged()
    {
        //
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        exportToXLS(new ArrayList<TrackSegment>(trackPool.getTrackSegmentList()));
    }

    private static void exportToXLS(List<TrackSegment> tracks)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select xls file.");

        int returnVal = chooser.showOpenDialog(null);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;

        exportToXLS(tracks, chooser.getSelectedFile());
    }

    public static boolean exportToXLS(List<TrackSegment> trackSegmentList, File file)
    {
        try
        {
            final WritableWorkbook doc = XLSUtil.loadWorkbookForWrite(file);
            final WritableSheet sh = XLSUtil.createNewPage(doc, "Tracks");
            int cursorY = 0;

            for (TrackSegment ts : trackSegmentList)
            {
                cursorY++;
                XLSUtil.setCellString(sh, 0, cursorY, "track #");
                XLSUtil.setCellNumber(sh, 1, cursorY, trackSegmentList.indexOf(ts));
                cursorY++;

                XLSUtil.setCellString(sh, 2, cursorY, "t");
                XLSUtil.setCellString(sh, 3, cursorY, "x");
                XLSUtil.setCellString(sh, 4, cursorY, "y");
                XLSUtil.setCellString(sh, 5, cursorY, "z");
                XLSUtil.setCellString(sh, 6, cursorY, "virtual");
                cursorY++;

                ArrayList<Detection> detectionList = ts.getDetectionList();

                for (int i = 0; i < detectionList.size(); i++)
                {
                    final Detection d = detectionList.get(i);

                    if (d.isEnabled())
                    {
                        XLSUtil.setCellNumber(sh, 2, cursorY, d.getT());
                        XLSUtil.setCellNumber(sh, 3, cursorY, d.getX());
                        XLSUtil.setCellNumber(sh, 4, cursorY, d.getY());
                        XLSUtil.setCellNumber(sh, 5, cursorY, d.getZ());
                        XLSUtil.setCellNumber(sh, 6, cursorY,
                                (d.getDetectionType() == Detection.DETECTIONTYPE_VIRTUAL_DETECTION) ? 1 : 0);

                        cursorY++;
                    }
                }
            }

            XLSUtil.saveAndClose(doc);
        }
        catch (Exception e)
        {
            if (Icy.getMainInterface().isHeadLess())
                IcyExceptionHandler.showErrorMessage(e, true);
            else
                MessageDialog.showDialog("Cannot open file.", MessageDialog.ERROR_MESSAGE);

            return false;
        }

        return true;
    }
}
